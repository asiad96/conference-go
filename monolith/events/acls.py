from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request

    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    # Create the URL for the request with the city and state

    payload = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    # Make the request

    response = requests.get(url, params=payload, headers=headers)

    # Pull out the data from the request
    photo_dict = json.loads(response.content)
    try:
        return photo_dict["photos"][0]["url"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    pass
